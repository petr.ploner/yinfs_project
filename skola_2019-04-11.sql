# ************************************************************
# Sequel Pro SQL dump
# Version 5437
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.12)
# Database: skola
# Generation Time: 2019-04-11 14:55:31 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table cv
# ------------------------------------------------------------

DROP TABLE IF EXISTS `cv`;

CREATE TABLE `cv` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `duration` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `cv` WRITE;
/*!40000 ALTER TABLE `cv` DISABLE KEYS */;

INSERT INTO `cv` (`id`, `duration`, `name`, `description`)
VALUES
	(2,'01/02-11/03','Pokladní','Brigáda při studiu, výpomoc v supermarketu'),
	(3,'06/04','Získání titulu ing.','Absolvování magisterského studia  na univerzitě Palackého v Olomouci'),
	(4,'04/04-12/05','Asistent prodeje','Asistent prodeje v knihkupectví'),
	(5,'01/06-12/12','Juniorský grant','Grant na výzkum pěstitelství ve stavu beztíže'),
	(6,'01/13-06/14','Lektor','Lektor výuky Univerzita Palackého v Olomouci'),
	(7,'06/14','Získání titulu PhD.','Univerzita Palackého v Olomouci'),
	(9,'07/14-nyní','Výzkuný pracovník','Akademie věd Brno');

/*!40000 ALTER TABLE `cv` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table gallery
# ------------------------------------------------------------

DROP TABLE IF EXISTS `gallery`;

CREATE TABLE `gallery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `file` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;

INSERT INTO `gallery` (`id`, `file`, `name`, `description`)
VALUES
	(2,'37c74827de833ef27155a2231c9a8f57.jpg','mimozemský talíř','Mimozemšťan'),
	(3,'a79c343e470e7f34def1702121f7a1ca.jpg','Noční planeta','Noční planeta abcd'),
	(4,'56aeb4ad449178a7798a0540ed9e0979.jpg','Vesmír','Vesmírný pohled na planetu'),
	(5,'38a92af9b4fa646b85d3e59b7d807991.jpg','Space X','Space X ve vesmíru');

/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table page_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `page_settings`;

CREATE TABLE `page_settings` (
  `key` varchar(50) NOT NULL DEFAULT '',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `page_settings` WRITE;
/*!40000 ALTER TABLE `page_settings` DISABLE KEYS */;

INSERT INTO `page_settings` (`key`, `value`)
VALUES
	('email','owner@example.com'),
	('facebook','#'),
	('motto','Nothing is impossible'),
	('page_name','Henry Novák'),
	('page_subtitle','Univerzita Palackého Olomouc'),
	('twitter','#');

/*!40000 ALTER TABLE `page_settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table pages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `pages`;

CREATE TABLE `pages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  `content` text COLLATE utf8_czech_ci NOT NULL,
  `title` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_czech_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;

INSERT INTO `pages` (`id`, `name`, `content`, `title`, `description`)
VALUES
	(1,'O mě','<article>\r\n<p>Jmenuji se Henry Novák, jsem vědec...</p><p>Vítr skoro nefouká a tak by se na první pohled mohlo zdát, že se balónky snad vůbec nepohybují. Jenom tak klidně levitují ve vzduchu. Jelikož slunce jasně září a na obloze byste od východu k západu hledali mráček marně, balónky působí jako jakási fata morgána uprostřed pouště. Zkrátka široko daleko nikde nic, jen zelenkavá tráva, jasně modrá obloha a tři křiklavě barevné&nbsp;<img src=\"https://cdn.pixabay.com/photo/2017/01/24/22/55/et-2006631_1280.jpg\" style=\"width: 50%; float: right;\" class=\"note-float-right\">pouťové balónky, které se téměř nepozorovatelně pohupují ani ne moc vysoko, ani moc nízko nad zemí. Kdyby pod balónky nebyla sytě zelenkavá tráva, ale třeba suchá silnice či beton, možná by bylo vidět jejich barevné stíny - to jak přes poloprůsvitné barevné balónky prochází ostré sluneční paprsky.</p><p>Jenže kvůli všudy přítomné trávě jsou stíny balónků sotva vidět, natož aby šlo rozeznat, jakou barvu tyto stíny mají. Uvidět tak balónky náhodný kolemjdoucí, jistě by si pomyslel, že už tu takhle poletují snad tisíc let. Stále si víceméně drží výšku a ani do stran se příliš nepohybují. Proti slunci to vypadá, že se slunce pohybuje k západu rychleji než balónky, a možná to tak skutečně je. Nejeden filozof by mohl tvrdit, že balónky se sluncem závodí, ale fyzikové by to jistě vyvrátili. Z fyzikálního pohledu totiž balónky působí zcela nezajímavě.<br></p><p><br></p>\r\n</article>','Já, Henry','abcdefgg'),
	(2,'Výzkum','<section>\r\n<p>Zabývám se především výzkumem vesmíru a možnostmi letu a obydlení planety Mars.</p><p>Sed non posuere lacus. Nunc non nibh ullamcorper, pulvinar ex id, posuere eros. Donec sodales est ipsum, sit amet tempor dui fringilla sed. Nullam quis consectetur odio, sed volutpat mauris. Ut aliquet, nisl sit amet ullamcorper interdum, est ligula interdum ante, et auctor dolor erat nec nisl. Aenean rutrum sapien sit amet magna bibendum, sed consectetur mauris tincidunt. Praesent suscipit quis arcu sit amet tincidunt. Donec commodo justo vel quam vulputate feugiat.</p><p>Aliquam non tempus mi. Mauris vestibulum nunc vel tellus maximus, vel dignissim ex fringilla. Curabitur magna magna, tristique eget sollicitudin ut, volutpat sit amet orci. Vivamus mattis eleifend rhoncus. Aliquam in velit a sem faucibus ullamcorper. Etiam sagittis bibendum purus quis pulvinar. Mauris semper sit amet ipsum vel suscipit. Duis non aliquet eros. Aliquam mollis tincidunt egestas.</p><p>Donec ut luctus lectus. Nam at ex at leo consectetur interdum. Duis eu interdum enim. Aenean maximus, arcu eget rhoncus lacinia, diam enim aliquam erat, nec cursus ligula mauris eget enim. Pellentesque vulputate luctus risus, ut dapibus nulla bibendum sed. Vestibulum vitae accumsan nulla, quis malesuada metus. In gravida leo eget turpis fermentum gravida. Nunc diam massa, mollis et vehicula in, auctor id tellus. Pellentesque mollis massa augue, eu iaculis nulla maximus sit amet. Nulla ultrices arcu enim.</p><p>Cras lacus turpis, tempus nec justo tincidunt, rutrum porttitor nisi. Morbi laoreet lobortis leo. Aenean vehicula sem enim, ac tincidunt tortor porta nec. Praesent placerat, ex a consequat pulvinar, lectus ex sollicitudin urna, nec ultricies tortor elit sit amet nisi. Etiam cursus felis turpis, quis hendrerit lorem elementum sit amet. Mauris placerat libero a sem semper commodo. Morbi purus arcu, ultrices vitae dui eu, pharetra egestas odio.</p><h2>Výzkumné práce, kterých jsem se zůčastnil</h2>\r\n</section>\r\n<article class=\"beautify-list\">\r\n<ul><li><span class=\"highlight\">2018</span>&nbsp;<b>Alternativní zdroje energie, náš způsob přežití</b><span style=\"font-size: 19.2px;\">&nbsp;-&nbsp;</span><span style=\"font-size: 12px;\">Studium možností získávání elektrické energie z alternativních zdrojů s dlouhodobou životností - studie pro možnost zakládání kolonie na planetě Mars.</span></li><li><span class=\"highlight\">2016</span>&nbsp;<b>Recyklace biologického odpadu ve vesmíru</b><span style=\"font-size: 19.2px;\">&nbsp;- </span><span style=\"font-size: 12px;\">Jak recyklovat bioodpad pro znovupoužití při letu na Mars.</span></li><li><span class=\"highlight\">2013</span>&nbsp;<b>Podmínky pro přežití ve stavu beztíže</b> - <span style=\"font-size: 12px;\">Studium vlivu dlouhého letu na zdravotní stav člověka.</span></li></ul>\r\n</article>','Výzkum','Výzkumné práce, na kterých pracuji, nebo jsem pracoval'),
	(3,'Publikace','<article class=\"beautify-list\">\r\n<ul><li><span class=\"highlight\">2015</span>&nbsp;<b>Článek</b>&nbsp;- Zabije nás vesmírná radiace dříve, než tam doletíme?</li><li><span class=\"highlight\">2014</span>&nbsp;<b>Disertační práce</b> - Budování ekosystému v příbytku na planetě Mars</li><li><span class=\"highlight\">2007</span>&nbsp;<b>Článek</b> - Opravdu jsme ve vesmíru sami?</li><li><span class=\"highlight\">2006</span>&nbsp;<b>Studie</b>: Pěstování brambor ve stavu beztíže</li><li><span class=\"highlight\">2004</span> <b>Diplomová práce</b> - Podmínky k přežití letu na planetu Mars</li><li><span class=\"highlight\">2000</span><b>&nbsp;Bakalářská práce</b> - Existuje život na planetě Mars?</li></ul>\r\n</article>','Publikace','Publikoval jsem již několik dokumentů'),
	(4,'Galerie','<p style=\"margin-bottom: 15px; padding: 0px; text-align: justify; font-family: \" open=\"\" sans\",=\"\" arial,=\"\" sans-serif;=\"\" font-size:=\"\" 14px;\"=\"\">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque ligula magna, vulputate eget euismod sed, feugiat et magna. In consequat orci tellus. Suspendisse potenti. Nam diam sapien, viverra a nulla nec, tempor ultricies ex. Aenean lacus metus, aliquet ut mi sed, fermentum lobortis tellus. Mauris a tristique purus. Nam eu molestie metus. Aliquam risus urna, efficitur sed dapibus eget, consectetur at sem. Cras eleifend velit vitae finibus volutpat. Nam imperdiet purus interdum eros tempor, sed vehicula mauris dictum. Maecenas hendrerit arcu massa, fringilla rutrum odio varius non.</p>\r\n','Galerie výsledků prací','Zde se můžete podívat na výsledky mých prací'),
	(5,'Kontakt','<article>\r\n<p>Pokud máte zájem semnou spolupracovat, nebo se zeptat na něco k výsledkům mého výzkumu, můžete využít několik možností, jak mě kontaktovat:&nbsp;</p>\r\n<div class=\"important-links\">\r\n<p><i class=\"fab fa-facebook-square\"></i>&nbsp;<a href=\"https://facebook.com\" target=\"_blank\">Henry Novák</a><br>\r\n<i class=\"fab fa-twitter-square\"></i> <a href=\"https://twitter.com\" target=\"_blank\">@HenryNovak</a><br>\r\n<i class=\"fas fa-envelope-square\" style=\"\">&nbsp;</i><a href=\"mailto:%20henry.novak@example.com\" style=\"\" open=\"\" sans\",=\"\" serif;=\"\" font-weight:=\"\" 400;\"=\"\">henry.novak@example.com</a></p>\r\n</div>\r\n</article>','Kontaktujte mě',''),
	(6,'Úvod','<article>\r\n<p>Vítejte na mém webu.</p><p>Vítr skoro nefouká a tak by se na první pohled mohlo zdát, že se balónky snad vůbec nepohybují. Jenom tak klidně levitují ve vzduchu. Jelikož slunce jasně září a na obloze byste od východu k západu hledali mráček marně, balónky působí jako jakási fata morgána uprostřed pouště. Zkrátka široko daleko nikde nic, jen zelenkavá tráva, jasně modrá obloha a tři křiklavě barevné pouťové balónky, které se téměř nepozorovatelně pohupují ani ne moc vysoko, ani moc nízko nad zemí. Kdyby pod balónky nebyla sytě zelenkavá tráva, ale třeba suchá silnice či beton, možná by bylo vidět jejich barevné stíny - to jak přes poloprůsvitné barevné balónky prochází ostré sluneční paprsky.</p><p><img src=\"https://cdn.pixabay.com/photo/2014/07/06/09/26/laboratory-385349_1280.jpg\" style=\"width: 50%; float: left; margin-right: 10px\" class=\"note-float-left\"><br></p><p>Jenže kvůli všudy přítomné trávě jsou stíny balónků sotva vidět, natož aby šlo rozeznat, jakou barvu tyto stíny mají. Uvidět tak balónky náhodný kolemjdoucí, jistě by si pomyslel, že už tu takhle poletují snad tisíc let. Stále si víceméně drží výšku a ani do stran se příliš nepohybují. Proti slunci to vypadá, že se slunce pohybuje k západu rychleji než balónky, a možná to tak skutečně je. Nejeden filozof by mohl tvrdit, že balónky se sluncem závodí, ale fyzikové by to jistě vyvrátili. Z fyzikálního pohledu totiž balónky působí zcela nezajímavě.</p>\r\n</article>','','');

/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `email` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  `password` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  `name` varchar(255) COLLATE utf8_czech_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`email`, `password`, `name`)
VALUES
	('owner@example.com','$2y$10$HkiO/gTEUvfydZiaa9JEd.F8pkhWTLTxPhG5bas.jXTMgQJidpp2m','Owner');

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
