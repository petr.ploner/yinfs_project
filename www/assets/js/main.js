$(function () {
    $('a.menuCollapser').click(function () {
        let menu = $('nav.leftColumn');
        menu.toggleClass('d-flexi');
    });

    if ($.summernote) {
        $('.summernote').summernote();
    }

    fancyBox();

    $('a.openDescription').click(function (e) {
        e.preventDefault();
        let description = $(this).parent().parent().children('p');
        if (description.is(':visible')) {
            description.hide(500);
        } else {
            description.show(500);
        }
    });
});

//Fancybox demo example, see: http://fancyapps.com/fancybox/3/
function fancyBox() {
    let imgOpts = $.extend(true, {}, $.fancybox.defaults, {
        caption: function (instance, item) {
            return $(this).next('figcaption').html();
        }
    });

    function applyImgOpts() {
        $('[data-fancybox="images"]').fancybox(imgOpts);
    }

    $("#imgOpts select").change(function () {
        let opt = $(this).attr("id");
        let val = $(this).val();

        imgOpts[opt] = val === "" ? false : val;

        if (opt === 'transitionEffect') {
            imgOpts['transitionDuration'] = opt === 'fade' ? 330 : 550;
        }

        applyImgOpts();
    });

    $("#imgOpts input[type=radio][name=lang]").on("change", function () {
        imgOpts.lang = $(this).val();

        applyImgOpts();
    });

    $("#imgOpts .toggle").change(function () {
        if (this.id === 'thumbs') {
            imgOpts.thumbs.autoStart = this.checked ? true : false;
        } else {
            imgOpts[this.id] = this.checked ? true : false;
        }

        applyImgOpts();
    });

    $("#imgOpts .buttons").change(function () {
        var buttonArr = $('input:checkbox:checked.buttons').map(function () {
            return this.value;
        }).get();

        buttonArr.push('close');

        imgOpts.buttons = buttonArr;

        applyImgOpts();
    });

    applyImgOpts();
}