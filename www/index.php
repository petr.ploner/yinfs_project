<?php
//hack for local cli server @see https://www.php.net/manual/de/features.commandline.webserver.php
if (php_sapi_name() == 'cli-server') {
    if (preg_match('/\.css|\.js|\.jpg|\.png|\.map$/', $_SERVER['REQUEST_URI'], $match)) {
        $mimeTypes = [
            '.css' => 'text/css',
            '.js' => 'application/javascript',
            '.jpg' => 'image/jpg',
            '.png' => 'image/png',
        ];
        $path = __DIR__.$_SERVER['REQUEST_URI'];
        if (is_file($path)) {
            header("Content-Type: {$mimeTypes[$match[0]]}");
            require $path;
            exit;
        }
    }
}

session_start();

require_once(__DIR__.'/../src/autoload.php');

$path = explode('/', ltrim(filter_input(INPUT_SERVER, 'REQUEST_URI'), '/'));
$router = new \Controller\Router();
$router->route($path);