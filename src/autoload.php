<?php

class Autoloader
{
    static public function loader($className)
    {
        $className = str_replace('\\', '/', $className);
        require __DIR__.'/'.$className.'.php';
    }
}

spl_autoload_register('Autoloader::loader');