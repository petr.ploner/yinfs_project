<?php

class Config
{
    const MYSQL_HOST = 'localhost';
    const MYSQL_USER = 'root';
    const MYSQL_PASSWORD = 'root';
    const MYSQL_PORT = 3306;
    const MYSQL_DB = 'skola';
}