<?php

namespace Model\Mapper;


use Model\Db\BasePdo;

class PageSettingsMapper extends BaseMapper
{
    public function findSettingsByKeys(array $keys): array
    {
        foreach ($keys as &$key) {
            $key = $this->pdo->quote($key);
        }
        $keys = implode(',', $keys);
        $stmt = $this->pdo->prepare(
            "SELECT `key`,`value`
                       FROM page_settings
                       WHERE `key` IN ($keys)
            "
        );
        $stmt->execute();

        return $stmt->fetchAll(BasePdo::FETCH_KEY_PAIR);
    }

    public function findAll(): array
    {
        $stmt = $this->pdo->prepare(
            "SELECT `key`,`value`
                       FROM page_settings
            "
        );
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function update(string $key, string $value)
    {
        $stmt = $this->pdo->prepare('UPDATE page_settings SET `value` = :value WHERE `key` = :key');
        $stmt->bindParam('key', $key);
        $stmt->bindParam('value', $value);
        $stmt->execute();
    }
}