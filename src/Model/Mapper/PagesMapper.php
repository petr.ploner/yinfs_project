<?php

namespace Model\Mapper;


use Model\Db\BasePdo;

class PagesMapper extends BaseMapper
{
    public function findById(int $id): ?array
    {
        $stmt = $this->pdo->prepare(
            "SELECT *
                       FROM pages
                       WHERE id = :id
            "
        );
        $stmt->bindParam('id', $id);
        $stmt->execute();

        return $stmt->fetch();
    }

    public function findAll(): array
    {
        $stmt = $this->pdo->prepare(
            "SELECT *
                       FROM pages
            "
        );
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function update(int $id, string $name, string $content, ?string $title, ?string $description)
    {
        $stmt = $this->pdo->prepare(
            'UPDATE pages SET `name` = :name, `content` = :content, `title` = :title, `description` = :description
                       WHERE `id` = :id'
        );
        $stmt->bindParam('name', $name);
        $stmt->bindParam('content', $content);
        $stmt->bindParam('title', $title);
        $stmt->bindParam('description', $description);
        $stmt->bindParam('id', $id);
        $stmt->execute();
    }
}