<?php

namespace Model\Mapper;


use Model\Db\BasePdo;

class UserMapper extends BaseMapper
{
    public function findByEmail(string $email): ?array
    {
        $stmt = $this->pdo->prepare(
            "SELECT *
                       FROM `users`
                       WHERE `email` = :email 
            "
        );

        $stmt->bindParam(':email', $email);

        $stmt->execute();
        $result = $stmt->fetch();

        return is_bool($result) ? null : $result;
    }

    public function updateUser(string $email, string $password)
    {
        $stmt = $this->pdo->prepare(
            "UPDATE `users` 
                       SET `password` = :password WHERE `email` = :email"
        );

        $stmt->bindParam('email', $email);
        $stmt->bindParam('password', $password);
        $stmt->execute();
    }

}