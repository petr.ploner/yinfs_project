<?php

namespace Model\Mapper;

use Model\Db\PdoWrapper;

abstract class BaseMapper
{
    /** @var \PDO */
    protected $pdo;

    public function __construct()
    {
        $this->pdo = PdoWrapper::getDbInstance();
    }

}