<?php

namespace Model\Mapper;


use Model\Db\BasePdo;

class CvMapper extends BaseMapper
{
    public function findById(int $id): ?array
    {
        $stmt = $this->pdo->prepare(
            "SELECT *
                       FROM cv
                       WHERE id = :id
            "
        );
        $stmt->bindParam('id', $id);
        $stmt->execute();

        return $stmt->fetch();
    }

    public function findAll(): array
    {
        $stmt = $this->pdo->prepare(
            "SELECT *
                       FROM cv
                       ORDER BY id DESC
            "
        );
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function update(int $id, string $name, string $description, string $duration)
    {
        $stmt = $this->pdo->prepare(
            'UPDATE cv SET `name` = :name, `description` = :description, `duration` = :duration
                       WHERE `id` = :id'
        );
        $stmt->bindParam('name', $name);
        $stmt->bindParam('description', $description);
        $stmt->bindParam('duration', $duration);
        $stmt->bindParam('id', $id);
        $stmt->execute();
    }

    public function insert(string $name, string $description, string $duration)
    {
        $stmt = $this->pdo->prepare(
            '
            INSERT INTO cv (`name`, `duration`, `description`)
            VALUES (:name, :duration, :description)
        '
        );
        $stmt->bindParam('name', $name);
        $stmt->bindParam('duration', $duration);
        $stmt->bindParam('description', $description);
        $stmt->execute();
    }

    public function delete(int $id)
    {
        $stmt = $this->pdo->prepare('DELETE FROM cv WHERE id = :id');
        $stmt->bindParam('id', $id);
        $stmt->execute();
    }
}