<?php

namespace Model\Mapper;


use Model\Db\BasePdo;

class GalleryMapper extends BaseMapper
{
    public function findById(int $id): ?array
    {
        $stmt = $this->pdo->prepare(
            "SELECT *
                       FROM gallery
                       WHERE id = :id
            "
        );
        $stmt->bindParam('id', $id);
        $stmt->execute();

        return $stmt->fetch();
    }

    public function findAll(): array
    {
        $stmt = $this->pdo->prepare(
            "SELECT *
                       FROM gallery
            "
        );
        $stmt->execute();

        return $stmt->fetchAll();
    }

    public function update(int $id, string $name, string $description)
    {
        $stmt = $this->pdo->prepare(
            'UPDATE gallery SET `name` = :name, `description` = :description
                       WHERE `id` = :id'
        );
        $stmt->bindParam('name', $name);
        $stmt->bindParam('description', $description);
        $stmt->bindParam('id', $id);
        $stmt->execute();
    }

    public function insert(string $name, string $file, string $description)
    {
        $stmt = $this->pdo->prepare(
            '
            INSERT INTO gallery (`name`, `file`, `description`)
            VALUES (:name, :file, :description)
        '
        );
        $stmt->bindParam('name', $name);
        $stmt->bindParam('file', $file);
        $stmt->bindParam('description', $description);
        $stmt->execute();
    }

    public function delete(int $id)
    {
        $stmt = $this->pdo->prepare('DELETE FROM gallery WHERE id = :id');
        $stmt->bindParam('id', $id);
        $stmt->execute();
    }
}