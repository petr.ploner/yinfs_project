<?php

namespace Model\Db;

class PdoWrapper
{
    protected static $db;
    protected static $pdo;

    public function __construct()
    {
        $dsn = 'mysql:host='.\Config::MYSQL_HOST.';dbname='.\Config::MYSQL_DB.';charset=utf8';
        self::$pdo = new BasePdo(
            $dsn, \Config::MYSQL_USER, \Config::MYSQL_PASSWORD, [
                    BasePdo::ATTR_ERRMODE => BasePdo::ERRMODE_EXCEPTION,
                    BasePdo::ATTR_DEFAULT_FETCH_MODE => BasePdo::FETCH_ASSOC,
                    BasePdo::ATTR_EMULATE_PREPARES => false,
                ]
        );
    }

    public static function getDbInstance(): \PDO
    {
        if (empty(self::$db)) {
            self::$db = new self;
        }

        return self::$pdo;
    }
}