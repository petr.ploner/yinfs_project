<?php

namespace Model\Service;


use Model\Exception\ImageTypeErrorException;
use Model\Mapper\GalleryMapper;

class GalleryService
{

    const UPLOAD_DIR = __DIR__.'/../../../www/assets/img/gallery';
    const ALLOWED_IMAGE_TYPES = ['jpg', 'png', 'gif', 'jpeg'];
    private $galleryMapper;

    public function __construct()
    {
        $this->galleryMapper = new GalleryMapper();
    }

    public function uploadImg($img, $name, $description)
    {
        $file = self::UPLOAD_DIR.basename($img['name']);
        $tmpName = explode('.', $img['name']);
        $fileName = md5(time().$tmpName[0]).'.'.end($tmpName);
        $imageFileType = pathinfo($file, PATHINFO_EXTENSION);

        if (!in_array($imageFileType, self::ALLOWED_IMAGE_TYPES)) {
            throw new ImageTypeErrorException();
        }

        if (!move_uploaded_file($img["tmp_name"], self::UPLOAD_DIR.'/'.$fileName)) {
            throw new ImageTypeErrorException();
        }

        $this->galleryMapper->insert($name, $fileName, $description);
    }

    public function delete($id)
    {
        $img = $this->galleryMapper->findById($id);
        unlink(self::UPLOAD_DIR.'/'.$img['file']);
        $this->galleryMapper->delete($id);
    }
}