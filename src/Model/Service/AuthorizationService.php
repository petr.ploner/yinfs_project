<?php

namespace Model\Service;

use Model\Exception\NotAuthorizedException;
use Model\Exception\WrongPasswordException;
use Model\Helper\PasswordHelper;
use Model\Helper\SessionHelper;
use Model\Mapper\UserMapper;

class AuthorizationService
{

    private $userMapper;

    public function __construct()
    {
        $this->userMapper = new UserMapper();
    }

    public function authorize(string $email, string $password): void
    {
        $user = $this->userMapper->findByEmail($email);
        if ($user === null || !password_verify($password, $user['password'])) {
            throw new NotAuthorizedException();
        }

        SessionHelper::set('email', $user['email']);
        SessionHelper::set('name', $user['name']);
        SessionHelper::set('logged', true);
    }

    public function logout()
    {
        session_destroy();
    }

    public function changeUserPassword(string $email, string $oldPassword, string $newPassword)
    {
        $user = $this->userMapper->findByEmail($email);
        if (!password_verify($oldPassword, $user['password'])) {
            throw new WrongPasswordException();
        }

        $this->userMapper->updateUser($email, PasswordHelper::encrypt($newPassword));
    }

}