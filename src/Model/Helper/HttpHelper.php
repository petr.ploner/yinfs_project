<?php

namespace Model\Helper;

class HttpHelper
{
    public static function isPost(): string
    {
        return $_SERVER['REQUEST_METHOD'] === 'POST';
    }
}