<?php

namespace Model\Helper;

class SessionHelper
{
    public static function get(string $key)
    {
        return $_SESSION[$key] ?? null;
    }

    public static function authorized(): bool
    {
        return self::get('logged') === true;
    }

    public static function set(string $key, $value)
    {
        $_SESSION[$key] = $value;
    }
}