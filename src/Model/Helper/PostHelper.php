<?php

namespace Model\Helper;

class PostHelper
{
    public static function get(string $key)
    {
        return filter_input(INPUT_POST, $key) ?? null;
    }

    public static function getInt($key): ?int
    {
        return filter_input(INPUT_POST, $key, FILTER_SANITIZE_NUMBER_INT) ?? null;
    }

    public static function getFloat($key): ?float
    {
        return filter_input(INPUT_POST, $key, FILTER_SANITIZE_NUMBER_FLOAT) ?? null;
    }

    public static function getMail($key): ?string
    {
        return filter_input(INPUT_POST, $key, FILTER_SANITIZE_EMAIL) ?? null;
    }
}