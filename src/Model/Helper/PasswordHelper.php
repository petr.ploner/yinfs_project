<?php

namespace Model\Helper;

class PasswordHelper
{
    public static function encrypt(string $password): string
    {
        return password_hash($password, PASSWORD_BCRYPT);
    }
}