<?php

namespace Controller;

class PublikaceController extends BaseController
{
    public function indexAction(array $params)
    {
        $page = $this->pagesMapper->findById(3);
        $this->description = $page['description'];
        $this->title = $page['title'];

        return [
            'name' => $page['name'],
            'content' => $page['content'],
        ];
    }
}