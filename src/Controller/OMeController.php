<?php

namespace Controller;

use Model\Mapper\CvMapper;

class OMeController extends BaseController
{
    private $cvMapper;

    public function __construct(string $actionName)
    {
        parent::__construct($actionName);
        $this->cvMapper = new CvMapper();
    }

    public function indexAction(array $params)
    {
        $page = $this->pagesMapper->findById(1);
        $this->description = $page['description'];
        $this->title = $page['title'];

        return [
            'name' => $page['name'],
            'content' => $page['content'],
            'cv' => $this->cvMapper->findAll(),
        ];
    }
}