<?php

namespace Controller;

use Model\Mapper\GalleryMapper;

class GalerieController extends BaseController
{
    private $galleryMapper;

    public function __construct(string $actionName)
    {
        parent::__construct($actionName);
        $this->galleryMapper = new GalleryMapper();
    }

    public function indexAction(array $params)
    {
        $page = $this->pagesMapper->findById(4);
        $this->description = $page['description'];
        $this->title = $page['title'];

        return [
            'name' => $page['name'],
            'content' => $page['content'],
            'gallery' => $this->galleryMapper->findAll(),
        ];
    }
}