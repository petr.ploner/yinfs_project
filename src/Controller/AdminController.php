<?php

namespace Controller;


use Model\Exception\ImageTypeErrorException;
use Model\Exception\NotAuthorizedException;
use Model\Exception\PasswordsDoesNotMatchException;
use Model\Exception\WrongPasswordException;
use Model\Helper\HttpHelper;
use Model\Helper\PasswordHelper;
use Model\Helper\PostHelper;
use Model\Helper\SessionHelper;
use Model\Mapper\CvMapper;
use Model\Mapper\GalleryMapper;
use Model\Mapper\UserMapper;
use Model\Service\AuthorizationService;
use Model\Service\GalleryService;

class AdminController extends BaseController
{
    private $authorizationService;
    private $galleryService;
    private $galleryMapper;
    private $cvMapper;

    public function __construct(string $actionName)
    {
        parent::__construct($actionName);
        $this->authorizationService = new AuthorizationService();
        $this->galleryService = new GalleryService();
        $this->galleryMapper = new GalleryMapper();
        $this->cvMapper = new CvMapper();
    }

    public function beforeRender(?string $action = null)
    {
        if ($action !== 'loginAction') {
            try {
                $this->securedRoute();
            } catch (NotAuthorizedException $e) {
                $this->redirect('admin/login');
            }
        }
    }

    public function loginAction()
    {
        $authError = null;
        if (HttpHelper::isPost()) {
            $email = PostHelper::getMail('email');
            $password = PostHelper::get('password');
            try {
                $this->authorizationService->authorize($email, $password);
                $this->redirect('admin');

            } catch (NotAuthorizedException $e) {
                $authError = 'Nesprávné přihlašovací údaje!';
            }
        }

        return [
            'authError' => $authError,
        ];
    }

    public function logoutAction()
    {
        $this->authorizationService->logout();
        $this->redirect('admin');
    }

    public function indexAction()
    {
        return [];
    }

    public function changePasswordAction()
    {
        $error = null;
        if (HttpHelper::isPost()) {
            try {
                if (PostHelper::get('password') === null || PostHelper::get('password') !== PostHelper::get('passwordConfirm')) {
                    throw new PasswordsDoesNotMatchException();
                }
                $this->authorizationService->changeUserPassword(
                    SessionHelper::get('email'),
                    PostHelper::get('currentPassword'),
                    PostHelper::get('password')
                );

                $this->redirect('admin');
            } catch (PasswordsDoesNotMatchException $e) {
                $error = 'Hesla se neshodují';
            } catch (WrongPasswordException $e2) {
                $error = 'Současné heslo není správné!';
            }
        }

        return [
            'error' => $error,
        ];
    }

    public function componentsAction()
    {
        return [
            'components' => $this->pageSettingsMapper->findAll(),
        ];
    }

    public function editComponentAction($parameters)
    {
        if (empty($parameters)) {
            Router::error(401);
        }

        $component = $this->pageSettingsMapper->findSettingsByKeys([$parameters[0]]);

        if (empty($component)) {
            Router::error(401);
        }

        if (HttpHelper::isPost()) {
            $this->pageSettingsMapper->update(key($component), PostHelper::get('value'));
            $this->redirect('admin/components');
        }

        return [
            'component' => $component,
        ];
    }

    public function pagesAction()
    {
        return [
            'pages' => $this->pagesMapper->findAll(),
        ];
    }

    public function editPageAction($parameters)
    {
        if (empty($parameters)) {
            Router::error(401);
        }

        $page = $this->pagesMapper->findById($parameters[0]);

        if (empty($page)) {
            Router::error(401);
        }

        if (HttpHelper::isPost()) {
            $this->pagesMapper->update(
                $page['id'],
                PostHelper::get('name'),
                PostHelper::get('content'),
                PostHelper::get('title'),
                PostHelper::get('description')
            );
            $this->redirect('admin/pages');
        }

        return [
            'page' => $page,
        ];
    }

    public function galleryAction()
    {
        return [
            'gallery' => $this->galleryMapper->findAll(),
        ];
    }

    public function addGalleryAction()
    {
        $error = null;
        if (HttpHelper::isPost()) {
            try {
                $this->galleryService->uploadImg(
                    $_FILES["photo"],
                    PostHelper::get('name'),
                    PostHelper::get('description')
                );
                $this->redirect('admin/gallery');
            } catch (ImageTypeErrorException $e) {
                $error = 'Při zpracování došlo k chybě, zkontrolujte, jeslti je soubor obrázek';
            }
        }

        return ['error' => $error];
    }

    public function editGalleryAction($parameters)
    {
        if (empty($parameters)) {
            Router::error(401);
        }

        $image = $this->galleryMapper->findById($parameters[0]);

        if (empty($image)) {
            Router::error(401);
        }

        if (HttpHelper::isPost()) {
            $this->galleryMapper->update(
                $image['id'],
                PostHelper::get('name'),
                PostHelper::get('description')
            );
            $this->redirect('admin/gallery');
        }

        return [
            'image' => $image,
        ];
    }

    public function deleteGalleryAction($parameters)
    {
        if (empty($parameters)) {
            Router::error(401);
        }
        $this->galleryMapper->delete($parameters[0]);
        $this->redirect('admin/gallery');
    }

    public function cvAction()
    {
        return [
            'cv' => $this->cvMapper->findAll(),
        ];
    }

    public function addCvAction()
    {
        if (HttpHelper::isPost()) {
            $this->cvMapper->insert(PostHelper::get('name'), PostHelper::get('description'), PostHelper::get('duration'));
            $this->redirect('admin/cv');
        }

        return [];
    }

    public function editCvAction($parameters)
    {
        if (empty($parameters)) {
            Router::error(401);
        }

        $cv = $this->cvMapper->findById($parameters[0]);

        if (empty($cv)) {
            Router::error(401);
        }

        if (HttpHelper::isPost()) {
            $this->cvMapper->update(
                $cv['id'],
                PostHelper::get('name'),
                PostHelper::get('description'),
                PostHelper::get('duration')
            );
            $this->redirect('admin/cv');
        }

        return [
            'cv' => $cv
        ];
    }

    public function deleteCvAction($parameters)
    {
        if (empty($parameters)) {
            Router::error(401);
        }
        $this->cvMapper->delete($parameters[0]);
        $this->redirect('admin/cv');
    }

}