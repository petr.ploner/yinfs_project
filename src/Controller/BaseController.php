<?php

namespace Controller;


use Model\Exception\ActionNotFoundException;
use Model\Exception\NotAuthorizedException;
use Model\Exception\ProcessCompletedException;
use Model\Helper\SessionHelper;
use Model\Mapper\PageSettingsMapper;
use Model\Mapper\PagesMapper;

abstract class BaseController
{
    protected $title;
    protected $description;
    protected $pageSettingsMapper;
    protected $pagesMapper;

    public function __construct(string $actionName)
    {
        if (!method_exists($this, $actionName)) {
            throw new ActionNotFoundException();
        }

        if (method_exists($this, 'beforeRender')) {
            $this->beforeRender($actionName);
        }

        $this->pageSettingsMapper = new PageSettingsMapper();
        $this->pagesMapper = new PagesMapper();
    }

    public function process(string $actionName, array $parameters)
    {
        $variables = $this->$actionName($parameters);
        if (is_array($variables)) {
            $this->render($actionName, $variables);
        }

        throw new ProcessCompletedException();
    }

    private function render(string $action, array $variables)
    {
        //remove "Action" from $action variable
        $action = substr($action, 0, -6);
        //remove "Controller" from class name
        $className = explode('\\', get_class($this));
        $controllerName = substr($className[count($className) - 1], 0, -10);
        $_path = __DIR__.'/../Template/'.$controllerName.'/'.$action.'.phtml';

        if ($this->title !== null) {
            $variables['_title'] = $this->title;
        }

        if ($this->description !== null) {
            $variables['_description'] = $this->description;
        }

        if ($controllerName !== 'Admin') {
            $variables['_pageSettings'] = $this->pageSettingsMapper->findSettingsByKeys(
                [
                    'page_name',
                    'motto',
                    'facebook',
                    'twitter',
                    'email',
                    'page_subtitle',
                ]
            );
        } else {
            $variables['_authorized'] = SessionHelper::authorized();
        }

        extract($variables);
        require($_path);
        throw new ProcessCompletedException();
    }

    protected function securedRoute()
    {
        $authorized = SessionHelper::authorized() === true;

        if (!$authorized) {
            throw new NotAuthorizedException();
        }
    }

    protected function redirect($url)
    {
        header("Location: /$url");
        header("Connection: close");
        exit;
    }

}