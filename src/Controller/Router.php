<?php

namespace Controller;

use Model\Exception\ActionNotFoundException;
use Model\Exception\NotAuthorizedException;
use Model\Exception\ProcessCompletedException;

class Router
{
    public function route(array $params)
    {
        $controllerName = $this->dashesToCamelNotation(strtolower(!empty($params[0]) ? $params[0] : 'homepage'));
        $action = lcfirst($this->dashesToCamelNotation(strtolower($params[1] ?? 'index')));
        $parameters = [];
        if (count($params) > 2) {
            $parameters = array_slice($params, 2);
        }

        $fileName = __DIR__.'/'.str_replace('\\', '/', $controllerName.'Controller.php');

        $controllerName = '\\Controller\\'.ucfirst($controllerName).'Controller';
        $actionName = $action.'Action';

        if (!file_exists($fileName) || !class_exists($controllerName)) {
            self::error(404);
        }

        try {
            /** @var BaseController $controller */
            $controller = new $controllerName($actionName);
            $controller->process($actionName, $parameters);
        } catch (ActionNotFoundException $e) {
            self::error(404);
        } catch (NotAuthorizedException $e2) {
            self::error(403);
        } catch (ProcessCompletedException $e3) {
            exit;
        }

    }

    private function dashesToCamelNotation($text)
    {
        $sentence = str_replace('-', ' ', $text);
        $sentence = ucwords($sentence);
        $sentence = str_replace(' ', '', $sentence);

        return $sentence;
    }

    public static function error(int $code)
    {
        http_response_code($code);
        die('ERROR: '.$code);
    }
}