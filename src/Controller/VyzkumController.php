<?php

namespace Controller;

class VyzkumController extends BaseController
{
    public function indexAction(array $params)
    {
        $page = $this->pagesMapper->findById(2);
        $this->description = $page['description'];
        $this->title = $page['title'];

        return [
            'name' => $page['name'],
            'content' => $page['content'],
        ];
    }
}