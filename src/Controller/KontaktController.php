<?php

namespace Controller;

class KontaktController extends BaseController
{
    public function indexAction(array $params)
    {
        $page = $this->pagesMapper->findById(5);
        $this->description = $page['description'];
        $this->title = $page['title'];

        return [
            'name' => $page['name'],
            'content' => $page['content'],
        ];
    }
}