<?php

namespace Controller;

class HomepageController extends BaseController
{
    public function indexAction(array $params)
    {
        $page = $this->pagesMapper->findById(6);
        $this->description = $page['description'];
        $this->title = $page['title'];

        return [
            'name' => $page['name'],
            'content' => $page['content'],
        ];
    }
}